import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import logo from './logo.svg';
import './App.css';
import { test as testFunc } from './test';

function App(props) {
  const { hello, world } = props;
  const [state, setState] = useState(hello);

  const object = {
    hello: 'world',
    prettier: 'maybe good',
    test: testFunc,
  };

  const test = hello1 => {
    if (
      state === hello1 + hello ||
      (world === state + hello1 &&
        world !== state - hello1 &&
        state !== hello1 + hello &&
        world === state + hello1 &&
        world !== state - hello1)
    ) {
      return hello1;
    }

    if (state === hello1) return hello1 + 1;
    let hello2;
    switch (state) {
      case 5: {
        const a = 6;
        return a;
      }
      default:
        hello2 = 5;
    }
    return hello2;
  };

  const arr = [0, 1, 2];

  const test2 = val => val;

  useEffect(() => {
    setState('er');
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        {arr.map(item => (
          <>
            <p key={item}>{item}</p>
            <p key={item}>{item}</p>
          </>
        ))}
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          {test2()}
          {test()}
          {hello}
          {world}
          {state}
          {Object.keys(object).length}
          {test()}
          {/* Edit <code>src/App.js</code> and save to reload. */}
        </p>
        <a
          // className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <button type="button">hello</button>
      </header>
    </div>
  );
}

App.propTypes = {
  hello: PropTypes.string.isRequired,
  world: PropTypes.string.isRequired,
};

export default App;
